import RxCocoa
import RxSwift


/// An example of that type of restriction is if the observable emits non-thread safe objects, which cannot be sent across threads.
/// In that case, RxSwift will allow you to switch schedulers, but you would be violating the logic of the underlying code.


print("========  subscribeOn() ========")

/// The fruit is generated on the main thread, but it would be nice to move it to a background thread.
/// To create the fruit in a background thread, you’ll have to use subscribeOn.


print("======= observeOn() ========")




/* MainScheduler */

/// MainScheduler sits on top of the main thread. This scheduler is used to process changes on the user interface and perform other high-priority tasks.
/// As a general practice when developing applications on iOS, tvOS or macOS, long-running tasks should not be performed using this scheduler, so avoid things like server requests or other heavy tasks.
/// Additionally, if you perform side effects that update the UI, you must switch to the MainScheduler to guarantee those updates make it to the screen.
/// The MainScheduler is also used to perform all the computations when using Units, and more specifically, Driver. As discussed in an earlier chapter, Driver ensures the computation is always performed in the MainScheduler to give you the ability to bind data directly to the user interface of your application.

/* SerialDispatchQueueScheduler */

/// SerialDispatchQueueScheduler manages to abstract the work on a serial DispatchQueue.
/// This scheduler has the great advantage of several optimizations when using observeOn.
/// You can use this scheduler to process background jobs which are better scheduled in a serial manner. For example,
/// if you have an application talking with a single endpoint of a server (as in a Firebase or GraphQL application), you might want to avoid[…]”

/* ConcurrentDispatchQueueScheduler */

/// ConcurrentDispatchQueueScheduler, similar to SerialDispatchQueueScheduler, manages to abstract work on a DispatchQueue. The main difference this time is that instead of a serial queue, the scheduler uses a concurrent one.
/// This kind of scheduler isn’t optimized when using observeOn, so remember to account for that when deciding which kind of scheduler to use.
/// A concurrent scheduler might be a good option for multiple, long-running tasks that need to end simultaneously. Combining multiple observables with a blocking operator, so all results are combined together when ready, can prevent serial schedulers from performing at their best. Instead, a concurrent scheduler could perform multiple concurrent tasks and optimize the gathering of the results.


/* OperationQueueScheduler */

/// OperationQueueScheduler is similar to ConcurrentDispatchQueueScheduler, but instead of abstracting the work over a DispatchQueue, it performs the job over an NSOperationQueue.
/// Sometimes you need more control over the concurrent jobs you are running, which you can’t do with a concurrent DispatchQueue.
/// If you need to fine-tune the maximum number of concurrent jobs, this is the scheduler for the job. You can define maxConcurrentOperationCount to cap the number of concurrent operations to suit your application’s needs.
