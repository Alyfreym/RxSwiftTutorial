import RxSwift
import RxRelay


// =============== Subjects =============== //

/*
 Manually add new values onto an observable at runtime that will then be emitted to subscribers.
 What you want is something that can act as both an observable and as an observer. And that something is called a Subject.
 */


let disposeBag = DisposeBag()


/* PublishSubject */

/// Here you create a PublishSubject. It’s aptly named, because, like a newspaper publisher, it will receive information and then turn around and publish it to subscribers, possibly after modifying that information in some way first”
/// Starts empty and only emits new elements to subscribers.

let publishSubject = PublishSubject<String>.init()

publishSubject.onNext("Is anyone listening One?")


let publishSubscriptionOne = publishSubject
    .subscribe(onNext: { string in
        print("publishSubscriptionOne: ", string)
    })

let publishSubscriptionTwo = publishSubject
    .subscribe(onNext: { string in
        print("publishSubscriptionTwo: ", string)
    })


publishSubject.onNext("Is anyone listening Two?")



/* BehaviorSubject */

/// Behavior subjects work similarly to publish subjects, except they will replay the latest .next event to new subscribers.
/// Init with initial value


let behaviorSubject = BehaviorSubject(value: "Initial value")



/* ReplaySubject */

/// Replay subjects will temporarily cache, or buffer, the latest elements they emit, up to a specified size of your choosing. They will then replay that buffer to new subscribers.
/// The following marble diagram depicts a replay subject with a buffer size of 2. The first subscriber (middle line) is already subscribed to the replay subject (top line) so it gets elements as they’re emitted.
/// The second subscriber (bottom line) subscribes after 2), so it gets 1) and 2) replayed to it.
/// Он возвращается новый подписчикам bufferSize последних обьектов.


let replaySubject = ReplaySubject<String>.create(bufferSize: 2)


replaySubject.onNext("1")
replaySubject.onNext("2")
replaySubject.onNext("3")

let replaySubscriptionOne = replaySubject
    .subscribe(onNext: { string in
        print("replySubscriptionOne: ", string)
    })

replaySubject.onNext("4")
replaySubject.onNext("5")
replaySubject.onNext("6")

