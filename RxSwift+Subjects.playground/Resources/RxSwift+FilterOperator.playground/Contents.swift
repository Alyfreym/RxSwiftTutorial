import RxSwift

print("Start.......")

/* ignoreElements() */

/// Even though this batter can’t seem to hit the broad side of a barn and has clearly struck out, nothing is printed, because you’re ignoring all .next events.
/// It’s up to you to add a .completed event to this subject in order to let the subscriber be notified. Add this code to do that:

let disposeBag = DisposeBag()

let ignoreSubject = PublishSubject<String>()


ignoreSubject
    .ignoreElements()
    .subscribe { _ in
      print("ignoreSubject - You're out!")
    }
    .disposed(by: disposeBag)

ignoreSubject.onNext("ignoreSubject onNext")
ignoreSubject.onNext("ignoreSubject onNext")
ignoreSubject.onNext("ignoreSubject onNext")


/* elementAt() */

/// There may be times when you only want to handle the the nth (ordinal) element emitted by an observable, such as the third strike.
/// For that you can use elementAt, which takes the index of the element you want to receive, and it ignores everything else.
/// In the marble diagram, elementAt is passed an index of 1, so it only allows through the second element.”

let elementSubject = PublishSubject<String>()

elementSubject
    .element(at: 2)
    .subscribe(onNext: { event in
      print("elementSubject: ", event)
    })
    .disposed(by: disposeBag)

elementSubject.onNext("1")
elementSubject.onNext("2")
elementSubject.onNext("3")
elementSubject.onNext("4")


/* filter() */


/* skip() */


/* skipWhile() */


/* skipUntil(trigger) */

print("============ skipUntil() ============")

let skipUntilSubject   = PublishSubject<String>()
let skipTriggerSubject = PublishSubject<String>()

skipUntilSubject
    .skip(until: skipTriggerSubject)
    .subscribe(onNext: {
      print("Skip: ", $0)
    })
    .disposed(by: disposeBag)


skipUntilSubject.onNext("skip: 1")
skipUntilSubject.onNext("skip: 2")
skipTriggerSubject.onNext("skip: 3")
skipUntilSubject.onNext("skip: 4")
skipTriggerSubject.onNext("skip: 5")
skipUntilSubject.onNext("skip: 6")


/* take() */

/// Taking is the opposite of skipping. When you want to take elements, RxSwift has you covered.
/// The first taking operator you’ll learn about is take, which as shown in this marble diagram, will take the first of the number of elements you specified.



/* takeWhile() */

/// There’s also a takeWhile operator that works similarly to skipWhile, except you’re taking instead of skipping



/* takeUntil(trigger) */

/// Like skipUntil, there's also a takeUntil operator, shown in this marble diagram,
/// taking from the source observable until the trigger observable emits an element.



/* distinctUntilChanged() */

/// distinctUntilChanged only prevents contiguous duplicates. So the 2nd element is prevented because it’s the same as the 1st,
/// but the last item, also an A, is allowed through, because it comes after a different letter (B).
/// Можно сделать с своей логикой. Вызывается свой блок для тебя чтоб ты сравнил.

print("============ distinctUntilChanged() ============")

Observable.of("A", "A", "B", "B", "A")
  .distinctUntilChanged()
  .subscribe(onNext: {
    print("distinctUntilChanged: ", $0)
  })
  .disposed(by: disposeBag)
