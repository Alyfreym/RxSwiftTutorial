import RxSwift


/* startWith() */

/// Put inital value in to observer

let startWithObserver = Observable.of(2, 3, 4)
startWithObserver.startWith(50)
    .subscribe(onNext: { value in
        print("startWithObserver: ", value)
    })


/* concat() */

/// Contact one observer to another observer
/// concat is similar to the merge-operator. It's used to combine multiple observables,
/// but the way it differs in is that all emissions by the first source observable come before any emissions from the second source observable (and so forth, if there are more observables).

let concatObserverFirst  = Observable.of(1, 2, 3)
let concatObserverSecond = Observable.of(4, 5, 6)

let concatObserver = Observable.concat([concatObserverFirst, concatObserverSecond])

concatObserver
    .subscribe(onNext: { value in
        print("concatObserver: ", value)
    })


/* merge() */

/// The first operator on the list is merge. It should be pretty self-explanatory.
/// It lets you merge the emissions from multiple observables.
/// This output acts like a single observable.


/* combineLatest() */


let combineLatestLeft = PublishSubject<String>()
let combineLatestRight = PublishSubject<String>()

/// The next one is combineLatest. It is used to output a value that depends on items from different observables.
/// When an item is emitted by either one of the two source observables, the operator takes the latest values from both of the source observables.
/// The items are then passed through a closure which determines how to combine them.

let combineLatestObservable = Observable.combineLatest(combineLatestLeft, combineLatestRight, resultSelector: {
    lastLeft, lastRight in
    "\(lastLeft) \(lastRight)"
})

let combineLatestObservableDispose = combineLatestObservable.subscribe(onNext: { value in
    print("combineLatest: ", value)
})


print("combineLatest: > Sending a value to Left")

combineLatestLeft.onNext("combineLatest: Hello,")

print("combineLatest: > Sending a value to Right")

combineLatestRight.onNext("combineLatest: world")

print("combineLatest: > Sending another value to Right")

combineLatestRight.onNext("combineLatest: RxSwift")

print("combineLatest: > Sending another value to Left")

combineLatestLeft.onNext("combineLatest: Have a good day,")

combineLatestObservableDispose.dispose()



/* Zip */

/// The same as combile latest but waits new items from all observers and after fire to subscribers
/// You may find it annoying that the combineLatest repeats much of the same elements as it doesn't wait for all source observables to provide a new item.
/// Well, you're in luck, because that's exactly what zip does. You also provide a closure to this operator, similar to combineLatest, that specifies how to combine the items.




/* withLatestFrom() */

/// Create two subjects simulating button presses and text field input. Since the button carries no real data, you can use Void as an element type.
/// When button emits a value, ignore it but instead emit the latest value received from the simulated text field.
/// Simulate successive inputs to the text field, which is done by the two successive button presses.
/// Короче когда кнопка нажимается subscripion вызываетс с полсденими данными из withLatestFromTextField


print("\n\n\n ======= withLatestFrom() ========")
// 1
let withLatestFromButton = PublishSubject<Void>()
let withLatestFromTextField = PublishSubject<String>()

// 2
let withLatestFromObservable = withLatestFromButton.withLatestFrom(withLatestFromTextField)
_ = withLatestFromObservable.subscribe(onNext: { value in
    print("withLatestFrom: ", value)
})

// 3
withLatestFromTextField.onNext("Par")
withLatestFromTextField.onNext("Pari")
withLatestFromTextField.onNext("Paris")
withLatestFromButton.onNext(())
withLatestFromButton.onNext(())





print("\n\n\n ========== sample() =========")
/* sample() */
/// по моему делает тоже самое что и withLatestFrom

let sampleFromButton = PublishSubject<Void>()
let sampleTextField = PublishSubject<String>()

// 2
let sampleObservable = sampleTextField.sample(sampleFromButton)
_ = sampleObservable.subscribe(onNext: { value in
    print("sample: ", value)
})

// 3
sampleTextField.onNext("Sam")
sampleTextField.onNext("Sami")
sampleTextField.onNext("Samir")
sampleFromButton.onNext(())
sampleFromButton.onNext(())



print("\n\n\n========= amb() ==========")

/* amb() */

/// Ааа он короче берет только того кто первый отправил данные и игнорит остальных
/// The amb(_:) operator subscribes to left and right observables. It waits for any of them to emit an element, then unsubscribes from the other one.
/// After that, it only relays elements from the first active observable.
/// It really does draw its name from the term ambiguous: at first, you don’t know which sequence you’re interested in, and want to decide only when one fires.


let ambLeft = PublishSubject<String>()
let ambRight = PublishSubject<String>()

let ambObservable = ambLeft.amb(ambRight)
let ambDisposable = ambObservable.subscribe(onNext: { value in
    print(value)
})

ambRight.onNext("Copenhagen")
ambLeft.onNext("Lisbon")
ambLeft.onNext("London")
ambLeft.onNext("Madrid")
ambRight.onNext("Vienna")

ambDisposable.dispose()



/* switchLatest() */

print("\n\n\n========= switchLatest() ==========")

/// Короче ты указываешь ему какой обсервер слушать и он игнорит остальные
/// Notice the few output lines. Your subscription only prints items from the latest sequence pushed to the source observable. This is the purpose of switchLatest().

let switchLatestOne = PublishSubject<String>()
let switchLatestTwo = PublishSubject<String>()
let switchLatestThree = PublishSubject<String>()
    
let switchLatestSource = PublishSubject<Observable<String>>()


let switchLatestObservable = switchLatestSource.switchLatest()
let switchLatestDisposable = switchLatestObservable.subscribe(onNext: { value in
    print("switchLatest: ", value)
})

switchLatestSource.onNext(switchLatestOne)
switchLatestOne.onNext("Some text from sequence one")
switchLatestTwo.onNext("Some text from sequence two")

switchLatestSource.onNext(switchLatestTwo)
switchLatestTwo.onNext("More text from sequence two")
switchLatestOne.onNext("and also from sequence one")

switchLatestSource.onNext(switchLatestThree)
switchLatestTwo.onNext("Why don't you see me?")
switchLatestOne.onNext("I'm alone, help me")
switchLatestThree.onNext("Hey it's three. I win.")

switchLatestSource.onNext(switchLatestOne)
switchLatestOne.onNext("Nope. It's me, one!")

switchLatestDisposable.dispose()


/* reduce() */

/// Note: reduce(_:_:) produces its summary (accumulated) value only when the source observable completes.
/// Applying this operator to sequences that never complete won’t emit anything. This is a frequent source of confusion and hidden problems.
/// Он короче счиате все за раз

print("\n\n\n========= reduce() ==========")

let reduceSource = Observable.of(1, 3, 5, 7, 9)
let reduceObservable = reduceSource.reduce(0, accumulator: { summary, newValue in
    return summary + newValue
})
reduceObservable.subscribe(onNext: { value in
    print("reduce: ", value)
})


/* scan() */

/// На каждый элемент вызывает блок и хранит в себе послдение данные
/// You get one output value per input value. As you may have guessed, this value is the running total accumulated by the closure.
/// Each time the source observable emits an element, scan(_:accumulator:) invokes your closure. It passes the running value along with the new element, and the closure returns the new accumulated value.
/// Like reduce(_:_:), the resulting observable type is the closure return type.

print("\n\n\n========= scan() ==========")

let scanSource = Observable.of(1, 3, 5, 7, 9)
  
let scanObservable = scanSource.scan(0, accumulator: +)
scanObservable.subscribe(onNext: { value in
    print("scan: ", value)
})
