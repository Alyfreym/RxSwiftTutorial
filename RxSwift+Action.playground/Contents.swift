import RxSwift
import RxCocoa
import Action

/*
 * - A trigger event signals that it’s time to do something.
 * - A task is performed.
 * - Immediately, later (or maybe never!), some value results from performing this task
 */


/// Note: Each action executed is considered complete when the observable returned by your factory closure completes or errors.
/// This prevents starting multiple long-running actions. This behavior is handy with network requests, as you’ll see below.
/// Короче определенный блок задачи

let buttonAction: Action<Void, Void> = Action {
  print("Doing some work")
  return Observable.empty()
}

/* Manual execution */
buttonAction.execute()


/*

 let loginAction: Action<(String, String), Bool> = Action { credentials in
  let (login, password) = credentials
  // loginRequest returns an Observable<Bool>
  return networkLayer.loginRequest(login, password)
}
 
 
 let loginPasswordObservable = Observable.combineLatest(loginField.rx.text, passwordField.rx.text) {
   ($0, $1)
 }
 loginButton
   .withLatestFrom(loginPasswordObservable)
   .bind(to: loginAction.inputs)
   .disposed(by: disposeBag)
 
 
 
 loginAction.elements
   .filter { $0 } // only keep "true" values
   .take(1)       // just interested in first successful login
   .subscribe(onNext: {
     // login complete, push the next view controller
   })
   .disposed(by: disposeBag)
 
 
 
 
 loginAction
   .errors
   .subscribe(onError: { error in
     if case .underlyingError(let err) = error {
       // update the UI to warn about the error
     }
   })
   .disposed(by: disposeBag)
 
 */

