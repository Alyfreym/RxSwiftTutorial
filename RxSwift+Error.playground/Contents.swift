import RxSwift
import RxCocoa



/*  catchErrorJustReturn() */




/*  retry(_ maxAttemptCount:) */

/// This operator will repeat the observable an unlimited number of times until it returns successfully.
/// For example, if there’s no internet connection, this would continuously retry until the connection was available.
/// his might sound like a robust idea, but it’s resource-heavy, and it’s seldom recommended to retry for an unlimited number of times if there’s no valid reason for doing it.


/* func retryWhen(_ notificationHandler:) -> Observable<E> */

/// The important thing to understand is that notificationHandler is of type TriggerObservable. The trigger observable can be either a plain Observable or a Subject and is used to trigger the retry at arbitrary times.


/*
retryWhen { e in
 e.enumerated().flatMap { (attempt, error) -> Observable<Int> in
 
 }
}

*/




/* materialize() and dematerialize() */
/// Using this operator, you are able to transform implicit sequences, which are manipulated with proper operators and multiple handlers, into an explicit one, so the handler for onNext, onError and onCompleted can be a single function
/// Короче при помощи их можно легко настраивать логи

/*
 observableToLog.materialize()
   .do(onNext: { (event) in
     myAdvancedLogEvent(event)
   })
   .dematerialize()
 */
