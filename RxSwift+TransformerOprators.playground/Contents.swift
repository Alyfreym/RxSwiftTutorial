import RxSwift

struct Student {
    
  var score: BehaviorSubject<Int>
}

/* toArray() */

/// Use toArray to transform the elements in an array

print("================== toArray() ==================")

let disposeBag = DisposeBag()

Observable.of("A", "B", "C")
    .toArray()
    .subscribe({
        print("ToArray: ", $0)
    })
    .disposed(by: disposeBag)


/* map() */



/* flatMap() */

Observable.just(1)
    .flatMap({ value in
        return Observable.just(value)
    })
    .subscribe { event in
        print("flatMap event: ", event)
    }.dispose()


/* flatMapLatest() */

/// Он короче меняется всю свою структуру на последний обсервер и слушает только его.
///

/*
 example(of: "flatMapLatest") {

   let disposeBag = DisposeBag()

   let ryan = Student(score: BehaviorSubject(value: 80))
   let charlotte = Student(score: BehaviorSubject(value: 90))

   let student = PublishSubject<Student>()

   student
     .flatMapLatest {
       $0.score
     }
     .subscribe(onNext: {
       print($0)
     })
     .disposed(by: disposeBag)

   student.onNext(ryan)

   ryan.score.onNext(85)

   student.onNext(charlotte)

   ryan.score.onNext(95) // IGNORE THIS BECAUSE LATEST OBSERVER IS charlotte

   charlotte.score.onNext(100)
 }
 
 --- Example of: flatMapLatest ---
 80
 85
 90
 100

 */

Observable.of(1,2,3,4)
    .flatMapLatest({ value in
        return Observable.just(value)
    })
    .subscribe { event in
        print("flatMapLatest event: ", event)
    }.dispose()



/* materialize(); dematerialize() */
/// Короче когда у тебя есть внутренние observer в observer и у него вызываетсчя ошибка или ты отправляешь ошибку то у тебя publisher terminate чтоб этого не было
/// Ты вызываешь materialize() в observer который конвертирует все тиаы в event,  dematerialize() конвертиуерт обратно в observer который возвращает элементы. 
 
