import RxSwift

// =============== Traits =============== //

/*
 Traits are observables with a narrower set of behaviors than regular observables.
 Their use is optional; you can use a regular observable anywhere you might use a trait instead.
 Their purpose is to provide a way to more clearly convey your intent to readers of your code or consumers of your API.
 The context implied by using a trait can help make your code more intuitive.
 
 
  --- Traits can't error out.
  --- Traits are observed on main scheduler.
  --- Traits subscribe on main scheduler.
  --- Traits share side effects.
 
 The phrase "Shares side effects" means it uses share(replay: 1, scope: .whileConnected) while "shares computational resources" means it uses share(scope: .whileConnected).
 That tells you that sharing side effects will replay the last emitted value for every new subscription while sharing computational resources will not. Otherwise, they are the same.
 
 
 /*
  * Driver is a special observable with the same constraints as explained before, so it can’t error out. All processes are ensured to execute on the main thread, which avoids making
  * UI changes on background threads.
  *
  * Traits in general are an optional part of the framework; you’re not forced to use them. Feel free to stick to observables and subjects, while making sure you are creating the
  * right task in the right scheduler. But if you want some checks while compiling, and predictable rules when dealing with the UI, these components can be extremely powerful and
  * save you time. It's easy to forget to call .observeOn(MainScheduler.instance) and end up creating UI processes on a background thread.
  *
  *
  */
 
 
 */



/* Single Trait */

/// Singles will emit either a .success(value) or .error event. .success(value) is actually a combination of the .next and .completed events


print("========== Single() =========")

let disposeBag = DisposeBag()

enum FileReadError: Error {
    case fileNotFound, unreadable, encodingFailed
}

func loadText(from name: String) -> Single<String> {
    // 4
    return Single.create { single in
        
        let disposeBag = BooleanDisposable()

        // Load file with name
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            single(.success("Success"))
        })
        
        return disposeBag
    }
}


loadText(from: "name").subscribe { event in
    switch event {
    case .failure(let error):
        print("Single Error: \(error)")
    case .success(let name):
        print("Single Success: \(name) ")
    }
}.disposed(by: disposeBag)



print("========== Driver() ===========")

/// Driver is a special observable with the same constraints as explained before, so it can’t error out.
/// All processes are ensured to execute on the main thread, which avoids making UI changes on background threads

/*
let search = searchCityName.rx.text
  .filter { ($0 ?? "").characters.count > 0 }
  .flatMapLatest { text in
    return ApiController.shared.currentWeather(city: text ?? "Error")
        .catchErrorJustReturn(ApiController.Weather.empty)
  }
  .asDriver(onErrorJustReturn: ApiController.Weather.empty)”
*/



/* asDriver(onErrorDriveWith:) */
/// With this function, you can handle the error manually and return a new sequence generated for this purpose only.


/* asDriver(onErrorRecover:): */
/// Can be used alongside another existing Driver. This will come in play to recover the current Driver that just encountered an error.




/* bind(to) */




print("========== Signal() =========")

/// It can’t fail
/// Events are sharing only when connected
/// All events are delivered in the main scheduler
/// You might consider this trait as an alternative to Driver, but there’s an important detail you have to consider first: there’s no replay of the last event after subscription.



print("========== ControlProperty() ========")



print("========== ControlEvent() ========")
