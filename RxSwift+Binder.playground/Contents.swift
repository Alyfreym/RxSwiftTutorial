import RxSwift
import RxCocoa


/*
extension Reactive where Base: UILabel {
    /// Bindable sink for `text` property.
    public var text: Binder<Base, String?> {
        return UIBindingObserver(UIElement: self.base) { label, text in
            label.text = text
        }
    }
}
*/

/*
 myObservable
   .map { "new value is \($0)" }
   .bind(to: myLabel.rx.text )
   .disposed(by: bag)
 
 */
