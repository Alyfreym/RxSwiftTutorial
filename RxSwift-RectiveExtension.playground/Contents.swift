import RxSwift
import RxCocoa
import Foundation


/* How to create custom operators */

/// fileprivate var internalCache = [String: Data]()
/// extension ObservableType where E == (HTTPURLResponse, Data) {
///
/// }
/// func cache() -> Observable<E> {
///  return self.do(onNext: { (response, data) in
///    if let url = response.url?.absoluteString, 200 ..< 300 ~= response.statusCode {
///      internalCache[url] = data
///    }
///  })
/// }
/// return response(request: request).cache().map { (response, data) -> Data in
///
/// }
/// if let url = request.url?.absoluteString, let data = internalCache[url] {
///  return Observable.just(data)
/// }


